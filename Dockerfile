# Start from the official Ubuntu Bionic (18.04 LTS) image
FROM ubuntu:jammy
ENV DEBIAN_FRONTEND=noninteractive


# setup timezone
ENV TZ=Asia/Dubai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone


# Install essentials and a few comfort things
RUN apt-get update 
RUN apt-get install -y \
	vim \
	ssh \
	sudo \
	nano \
	ffmpeg \
	git \
	wget
RUN apt-get install -y \
	ca-certificates \
	build-essential \
	software-properties-common ;\
	rm -rf /var/lib/apt/lists/*




# Create a new user called foam
RUN useradd --user-group --create-home --shell /bin/bash foam ;\
	echo "foam ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers




# Install OpenFOAM v9 (without ParaView)
# including configuring for use by user=foam
# plus an extra environment variable to make OpenMPI play nice
RUN sh -c "wget -O - http://dl.openfoam.org/gpg.key | apt-key add -" ;\
	add-apt-repository http://dl.openfoam.org/ubuntu ;\
	apt-get update ;\
	apt-get install -y --no-install-recommends openfoam9 ;\
	rm -rf /var/lib/apt/lists/* ;\
	echo "source /opt/openfoam9/etc/bashrc" >> ~foam/.bashrc ;\
	echo "export OMPI_MCA_btl_vader_single_copy_mechanism=none" >> ~foam/.bashrc






USER root

# install python
RUN apt-get update && apt-get install -y --no-install-recommends \
    python3 \
    python3-pip

# do some cleaning up
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# install python packages
RUN pip3 install numpy
RUN pip3 install matplotlib
RUN pip3 install openturns==1.19.post1
RUN pip3 install platypus-opt==1.0.4




# set the default container user to foam
USER foam
